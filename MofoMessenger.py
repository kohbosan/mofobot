from xml.etree import ElementTree
from urllib2 import urlopen, URLError
from urllib import urlencode
from json import loads, dumps
from StringIO import StringIO
import re, telnetlib, random, time, messenger, logging, pprint, pickle


class MofoMessenger(messenger.Messenger) :
    def __init__( self, api_token, chat_id ) :
        messenger.Messenger.__init__(self, api_token, chat_id)
        self.url = "https://api.telegram.org/" + api_token

        self.logger = logging.getLogger("mofo")

        # Current update id offset for  TG updates
        self.update_id = "0"

        # List of words to watch for on slickdeals
        try :
            f = open("watchlist")
            self.watch_list = pickle.load(f)
            f.close()
        except (IOError, AssertionError, EOFError) :
            self.logger.warning("Failed to load watchlist, defaulting to empty list.")
            self.watch_list = [ ]
        self.logger.debug("Watchlist: " + pprint.pformat(self.watch_list))

        # Collection of the commands that get information and require no reply
        self.commands = {
            "/slickdeals" : [ self.getSlickDeals, "Returns a list of the popular deals on SlickDeals.net" ],
            "/watchsd"    : [ self.add_sd_watch, "Adds a new word to watch for on SlickDeals" ],
            "/delwatchsd" : [ self.del_sd_watch, "Removes word from SlickDeals watch list."],
            "/saymyname"  : [ self.sayName, "Prints your name"],
            "/help"       : [ self.sendHelp, "Prints this help text" ]
        }

        self.defaults = [
            "I can't do that, Dave.",
            "Come on, man. That's not one of my commands",
            "WRONG!",
            "Type /help for a list of commands"
        ]

        self.names = [
            "bitch"
        ]

    def getUpdates( self ) :
        url = self.url + "/getUpdates?offset=" + self.update_id
        self.logger.info("Getting updates from Telegram")
        self.logger.debug(url)
        updates = loads(urlopen(url).read().decode('utf-8'))
        if updates[ 'ok' ] :
            if updates[ 'result' ] :
                results = updates[ 'result' ]
                self.update_id = str(results[ -1 ][ 'update_id' ] + 1)
                return updates[ 'result' ]
            else :
                return [ ]
        else :
            self.logger.warning("Something went wrong...")

    def handleUpdates( self, updates ) :
        for update in updates :
            if "message" in update :
                message = update[ 'message' ]
                chat_id = str(message[ 'chat' ][ 'id' ])
                if "text" in message :
                    cmd = message[ 'text' ].split(' ')[ 0 ]
                    if cmd == "/start" :
                        self.sendHelp(update)
                    elif cmd in self.commands :
                        self.commands[ cmd ][ 0 ](update)
                    else :
                        r = random.randint(0, len(self.defaults) - 1)
                        self.send_tg_message(self.defaults[ r ], chat_id)

    def get_chat_id( self, update ) :
        return str(update[ 'message' ][ 'chat' ][ 'id' ])

    def get_message_text( self, update ) :
        return update[ 'message' ][ 'text' ]

    def get_name(self, update):
        return update['message']['from']['first_name']

    def send_tg_message( self, message, chat_id, echo = False ) :
        url = self.url + "/sendMessage"
        data = {
            "chat_id"                  : chat_id,
            "text"                     : message,
            "parse_mode"               : "Markdown",
            "disable_web_page_preview" : True
        }
        try :
            response = urlopen(url, data = urlencode(data)).read()
            self.logger.info("Sending message to chat #" + chat_id)
            if echo :
                self.logger.info(message)
            assert loads(response)[ 'ok' ] == True, response
        except URLError :
            self.logger.exception("Something went wrong with the connection.")
        except AssertionError :
            self.logger.exception("Something went wrong while sending message.")
        return bytes(dumps(data))

    def sendGreeting( self, update ) :
        pass

    def sendHelp( self, update ) :
        chat_id = self.get_chat_id(update)
        response = StringIO()
        response.write("I can understand these commands:\n\n")
        response.writelines(map(lambda c : c + " - " + self.commands[ c ][ 1 ] + "\n", self.commands))
        self.send_tg_message("Hey!\n\n" + response.getvalue(), chat_id)
        response.close()

    def getSlickDeals( self, update ) :
        chat_id = self.get_chat_id(update)
        deals = self.loadSlickDeals()
        deals_string = StringIO()
        try :
            assert len(deals) > 0
            deals_string.write("*Popular Slick Deals*\n")
            for deal in self.loadSlickDeals()[ 0 :7 ] :
                deals_string.write("*" + "-" * 20 + "*\n")
                deals_string.write("[{}]({})\n".format(deal[ 0 ], deal[ 1 ]))
        except AssertionError :
            deals_string.write("I can't get the deals right now.")
            self.logger.exception("Deals list is empty")
        self.send_tg_message(deals_string.getvalue(), chat_id)

    def loadSlickDeals( self ) :
        self.logger.info("Fetching deals from SlickDeals.net")
        deals = [ ]
        url = "http://slickdeals.net/newsearch.php?mode=popdeals&searcharea=deals&searchin=first&rss=1"
        try :
            response = urlopen(url).read()
            response = response.decode('ascii', 'ignore')
            tree = ElementTree.fromstring(response)
            for node in tree[ 0 ].iter('item') :
                title = re.sub('[\[,\]]', '', node[ 0 ].text)
                link = node[ 1 ].text
                deals.append((title, link))
        except URLError :
            self.logger.exception("Something went wrong fetching the deals.")
        return deals

    def makePoll( self ) :
        pass

    def add_sd_watch( self, update ) :
        chat_id = self.get_chat_id(update)
        try :
            word = self.get_message_text(update).split(' ')[ 1 ].lower().encode('utf-8')
            if word not in self.watch_list:
                self.watch_list.append(word)
                f = open("watchlist", "w")
                pickle.dump(self.watch_list, f, -1)
                f.close()
                self.send_tg_message("I added " + word + " to the watch list.", chat_id, True)
            else:
                self.send_tg_message(word.capitalize() + " is already in the watch list.", chat_id)
            self.logger.debug("Watchlist: " + pprint.pformat(self.watch_list))
        except IndexError :
            self.send_tg_message("Proper format is /watchsd word_to_watch", chat_id)
        except IOError :
            msg = "Couldn't add the word to the watchlist."
            self.logger.exception(msg)
            self.send_tg_message(msg, chat_id)

    def del_sd_watch(self, update):
        chat_id = self.get_chat_id(update)
        try:
            word = self.get_message_text(update).split(' ')[ 1 ].lower().encode('utf-8')
            if word in self.watch_list:
                self.watch_list.remove(word)
                f = open("watchlist", "w")
                pickle.dump(self.watch_list, f, -1)
                f.close()
                self.send_tg_message("Removed " + word + " from watch list.", chat_id, True)
            else:
                self.send_tg_message(word.capitalize() + " isn't in the watch list.", chat_id)
            self.logger.debug("Watchlist: " + pprint.pformat(self.watch_list))
        except IOError:
            msg = "Unable to remove word from the watchlist."
            self.logger.exception(msg)
            self.send_tg_message(msg, chat_id)

    def get_ts_info( self ) :
        host = "voice.kohding.net"
        port = 10011
        tn = telnetlib.Telnet(host, port)
        tn.write("login kohbo ps9PTFGs")
        print(tn.read_eager())
        tn.close()

    def load_watch_list( self ) :
        f = open("watchlist", "r")
        for line in f.readlines() :
            self.watch_list.append(line)
        f.close()

    def sayName(self, update):
        chat_id = self.get_chat_id(update)
        if self.get_name(update) == "Juan":
            msg = "Hey, Juan ;)"
        else:
            msg = "Hey, " + self.names[random.randint(0, len(self.names)-1)]
        self.send_tg_message(msg, chat_id)

if __name__ == "__main__" :
    logging.basicConfig(format = "[%(levelname)s] %(asctime)s %(message)s", level = logging.DEBUG)
    logger = logging.getLogger('mofo')
    msgr = MofoMessenger("bot187222137:AAGetYRhbmlsM-5YTh1UvkJByW0SlWKQRMY", "-1001033397670")
    logger.info("Clearing the pending updates before starting listen loop.")
    logger.debug(msgr.getUpdates())
    logger.info("Starting listen loop.")
    while True :
        updates = msgr.getUpdates()
        logger.debug("Updates:\n" + pprint.pformat(updates))
        msgr.handleUpdates(updates)
        time.sleep(3)
        # msgr.get_ts_info()
